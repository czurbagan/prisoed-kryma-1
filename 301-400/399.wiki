<metadesc>Присоединение Крыма к России. Рескрипты, письма, реляции и донесения. Том I. 1775—1777 гг. / Н. Дубровин, 1885. Письмо Шагин-Гирея — турецкому султану. Читать бесплатно, без регистрации, без рекламы.</metadesc>
{{Присоед-Крыма
|ТОМ=1
|КАЧЕСТВО=3
}}
__NOTOC__
<div class="text">
==Письмо Шагинъ-Гирея — турецкому султану (Приложеніе лит. К).==

{{p|593}}

Во всей вселенной славнѣйшему и могущественнѣйшему престолу усердное донесеніе о слѣдующемъ.

Чрезъ нѣсколько временъ опредѣленіемъ судьбины джингинской фамиліи султаны обыкновенно достигали своихъ желаній и возводились въ достоинство ханское отъ власти величайшихъ монарховъ. Но Провиденіемъ Всемогущаго Бога въ прошедшіе годы предоставлено то трактатомъ вѣчнаго мира на общую волю и единодушное согласіе татарскаго народа. Въ семъ положеніи первѣйшіе чиноначальники, живущіе въ Крыму, купно со всѣмъ принадлежащимъ къ сей области благонамѣреннымъ татарскимъ народомъ, послѣ нашего къ нимъ прибытія, на которое снизошли мы по убѣдительнѣйшимъ ихъ приглашеніямъ, приняли насъ общимъ согласіемъ въ ханы.

Когда препровожденные къ вашему высокомонаршему двору съ усерднѣйшимъ провозглашеніемъ изъявляющіе о томъ махзары достигнутъ и удостоятся повергнуты быть предъ священнымъ вашего величества престоломъ, то съ прошеніемъ на всенижайшія ихъ моленія милостиваго снисхожденія и высочайшаго благоволенія, къ монаршему скипетру сіе повергая, предаемъ великодушію и справедливости вашего величества.
</div>
{{^}}
{{Sub-nav| {{Prv}} | {{Nxt}} }}
