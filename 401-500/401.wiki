<metadesc>Присоединение Крыма к России. Рескрипты, письма, реляции и донесения. Том I. 1775—1777 гг. / Н. Дубровин, 1885. Письмо Шагин-Гирей-хана — верховному визирю Блистательной Порты. Читать бесплатно, без регистрации, без рекламы.</metadesc>
{{Присоед-Крыма
|ТОМ=1
|КАЧЕСТВО=3
}}
__NOTOC__
<div class="text">
==Письмо Шагинъ-Гирей-хана — верховному визирю Блистательной Порты. (Приложеніе лит. М).==

{{p|595}}

По случаю препровожденія нынѣ отъ крымскаго правительства и всего общества къ Блистательной Портѣ махзаровъ и нашего притомъ его величеству вѣнценосному монарху чистосердечное усердіе изъявляющаго приношенія, въ надѣяніи премудраго вашего споспѣшествованія и о благопринятіи оныхъ тщанія, написавъ сіе дружеское почтительнѣйшее къ вамъ препровождаемъ, уповая на Всемогущаго Бога, что вы означенное усердное наше приношеніе его величеству, покровителю калифовъ, съ должнымъ благоговѣніемъ поднеся, своимъ благоразумнымъ стараніемъ милостивое принятіе и апробацію исходатайствовать не оставите. Между тѣмъ прошу содержать насъ въ добродѣтельной вашей памяти и не исключать изъ числа своихъ доброжелателей. Впрочемъ ваша жизнь и счастіе да будутъ безконечны.
</div>
{{^}}
{{Sub-nav| {{Prv}} | {{Nxt}} }}
