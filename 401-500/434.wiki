<metadesc>Присоединение Крыма к России. Рескрипты, письма, реляции и донесения. Том I. 1775—1777 гг. / Н. Дубровин, 1885. Журналъ князя Прозоровского о происходящем в Крыму и на Кубани с 20 по 31 мая 1777 г. Читать бесплатно, без регистрации, без рекламы.</metadesc>
{{Присоед-Крыма
|ТОМ=1
|КАЧЕСТВО=3
}}
__NOTOC__
<div class="text">
==№ 198. Журналъ князя Прозоровскаго о происходящемъ въ Крыму и на Кубани.==

{{p|642}}

{{right2|<small>Съ 20-го по 31-е мая 1777 г.</small>}}

По прибытіи сюда г. генералъ-маіора и кавалера фонъ-Рейзера, яко старшаго предъ г. генералъ-маіоромъ Шестаковымъ, опредѣлилъ я къ командованію расположеннымъ къ сторонѣ Козлова деташементомъ, къ принятію коего со всѣми предшедшими о семъ посту повелѣніями и прочими письменными дѣлами и отправленъ онъ 20-го числа мая. Генералъ-же маіоръ Шестаковъ опредѣленъ въ корпусъ г. генералъ-поручика и кавалера Суворова.

Въ тотъ же день крейсерующій съ эскадрою на Черномъ морѣ флота г. капитанъ 2-го ранга и кавалеръ Карташовъ доносилъ мнѣ о благополучномъ съ оною эскадрою прибытіи 12-го числа мая на видъ городовъ Кинбурна и Очакова и что онъ отъ 17-го числа съ Очаковскаго рейда началъ опять продолжать крейсерство къ Козлову.

20-го числа его свѣтлость ханъ увѣдомилъ меня, что онъ, поруча часть нижеслѣдующей въ Крыму здѣсь набережности, яко то Сербулатскую пристань и Акмечеть, съ урочищемъ Карахаджи въ Тарханскомъ Кутѣ состоящіе, бдѣнію мурзы мансурской фамиліи Бей-Арслана и подчиня ему еще его братьевъ Арсланъ-шаха и Сары-мурзу съ шестьюдесятью человѣками татаръ, указываетъ своею инструкціею содержать на тѣхъ мѣстахъ караулы и посылать въ околичности своей разъѣзды, не попущая никому проѣзжать безъ письменнаго вида ни въ которую сторону и увѣдомлять о всѣхъ происшедшихъ {{p|643}} обстоятельствахъ находящагося тамъ командира россійскаго войска, подтверждая исправлять сіе порученіе усердно и неусыпно.

Я посему предложилъ г. генералъ-маіору Рейзеру, яко имѣющему въ томъ краю начальство, чтобы при взаимной его связи съ татарами приказалъ обходиться съ ними ласково и по увѣдомленіямъ упомянутаго ханскаго чиновника и во всемъ стараться соглашать поступь свою съ нимъ въ общихъ дѣлахъ. Для поощренія къ сему его велѣлъ пристойнымъ образомъ подарить на сто рублей.

21-го слѣдуетъ здѣсь подъ № 1-мъ рапортъ бригадира и кавалера Бринка отъ 13-го числа касательно до упражнявшагося въ разбоѣ Тохтамышъ-Гирей-султана съ его скопищемъ и какія онъ, г. Бринкъ, сдѣлалъ по тому распоряженія свои, а также и о прочемъ случившемся по тамошнему краю.

Подъ № 2-мъ присоединяется здѣсь предписаніе мое, данное ему, бригадиру Бринку, какъ на нынѣ полученный, такъ и на два еще помѣщенные уже въ прежнемъ журналѣ рапорта его.

Въ 23-й день разсудилъ я снабдить гг. частныхъ командировъ нѣкоторыми относительно до десанта примѣчаніями, слѣдующими здѣсь подъ № 3-мъ, предлагая въ примѣръ имъ бывшее въ послѣдній годъ прошедшей войны на здѣшній полуостровъ нашествіе турокъ, что сколь велики затрудненія въ высадкѣ на берегъ, такъ равно нужно имѣть осторожность, чтобы ложнаго десанта не почесть за истинный, а потому развѣ при крайней и настояще видимой въ превосходствѣ силъ непріятельскихъ нуждѣ звать къ себѣ другого на помощь.

25-го, флота капитанъ 2-го ранга Карташовъ рапортовалъ, что онъ 20-го числа отъ Очакова къ Козлову въ неближнее разстояніе прибылъ благополучно и видѣлъ тамъ стоящихъ на тотъ разъ на рейдѣ малыхъ купеческихъ судовъ подъ турецкими флагами 35, получающихъ въ нагрузку съ крымскихъ береговъ пшеницу и соль. Оттуда намѣренъ онъ, г. Карташовъ, былъ въ скоромъ времени отправиться съ эскадрою на видъ Балаклавскихъ береговъ. Но при томъ доноситъ, что ввѣренной ему {{p|644}} эскадры суда требуютъ отъ происшедшихъ штурмовъ малыхъ поправленій и чтобы я повелѣлъ для того не болѣе какъ сутокъ на трое войти ему со всею эскадрою въ Балаклавскую гавань. Почему я, позволя такой входъ, предписалъ однакоже, чтобы старался онъ какъ можно скорѣе исправляться, а между тѣмъ сколько есть неповрежденныхъ судовъ, то всѣ оныя для стражи и примѣчанія въ морѣ оставить на Балаклавскомъ рейдѣ.

27-го числа, при рапортѣ г. генералъ-маіора Борзова, получено письмо, присланное къ нему отъ бывшаго сообщникомъ некрасовцу Родіону Петрову, писаря ихъ Александра Алексѣева, по поводу посланнаго отъ него, г. Борзова, въ недавнѣ къ нимъ въ селеніи, которое и слѣдуетъ здѣсь въ копіи подъ № 4-мъ.

На оное 28-го числа, каковымъ я снабдилъ его, генералъ-маіора, предписаніемъ, оное означается у сего № 5-мъ, давъ о всемъ томъ знать и бригадиру Бринку, къ единому его свѣдѣнію и съ тѣмъ, дабы иногда въ сихъ дѣлахъ съ ними не сдѣлалъ чего противнаго настоящему объ нихъ положенію.

Въ тотъ же день имѣлъ я рапортъ бригадира Бринка, слѣдующій у сего № 6-мъ, о появившихся у значущихся тамъ султановъ скопищахъ и разноплеменной горской сволочи, съ намѣреніемъ разорять добронамѣренныхъ и предавшихся хану татаръ, а затѣмъ и какія на отряженіе сихъ покушающихся предприняты имъ, г. Бринкомъ, надобныя мѣры. Я посылалъ для разсмотрѣнія сей его рапортъ къ хану, съ которымъ соображая онъ письмо, дошедшее къ нему отъ Арсланъ-Гирей-султана, прислалъ и ко мнѣ оное, переводъ съ котораго препроводилъ я къ бригадиру Бринку, при повелѣніи здѣсь подъ № 7-мъ, въ копіи представляемомъ и съ переводомъ самаго того письма подъ № 8-мъ подносимаго<ref>Приложеніе это по неимѣнію значенія не помѣщается.</ref>.

29-го числа получилъ я отъ его свѣтлости записку, писанную къ находящемуся при немъ агѣ отъ козловскаго каймакана объ извѣстіяхъ царьградскихъ, которую здѣсь въ переводѣ подъ № 9-мъ и поднести честь имѣю. {{p|645}}

Въ 31-й день получилъ я рапортъ чрезъ Балаклаву флота отъ г. капитана 2-го ранга и кавалера Карташова, что онъ съ эскадрою своею 30-го числа прибылъ на сей рейдъ.

==Примѣчанія==
<references/>
</div>
{{^}}
{{Sub-nav| {{Prv}} | {{Nxt}} }}
